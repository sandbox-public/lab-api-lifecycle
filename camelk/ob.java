import org.apache.camel.BindToRegistry;
import javax.sql.DataSource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;


public class ob extends org.apache.camel.builder.RouteBuilder {

    @BindToRegistry
    public DataSource connectionFactory() {

        DriverManagerDataSource ds = new DriverManagerDataSource();
        ds.setDriverClassName("org.h2.Driver");
        ds.setUsername("sa");
        ds.setPassword("");
        ds.setUrl("jdbc:h2:mem:testdb;INIT=runscript from 'classpath:schema.sql'");

        return ds;
    }

    @Override
    public void configure() throws Exception {

        from("direct:get-banks")
            // .log("test prop: {{test.prop}}")
            .log("got get-banks request...")
            .to("sql:SELECT * FROM usecase.banks;")
            .to("dataformat:json-jackson:marshal?prettyPrint=true");

        from("direct:get-banks-by-id")
            // .log("test prop: {{test.prop}}")
            .log("got get-banks-by-id request...")
            .to("sql:SELECT * FROM usecase.banks where id=:#${header.bank_id};")
            .to("dataformat:json-jackson:marshal?prettyPrint=true");
    }
}