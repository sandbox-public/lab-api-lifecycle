# Running the Integration with CamelK

To run the CamelK integration directly in dev mode, execute:

```
kamel run \
--dev \
--name ob \
--property camel.rest.port=8080 \
--dependency=camel-rest \
--dependency camel-undertow \
--dependency camel-sql \
--dependency camel-jackson \
--dependency mvn:org.springframework.boot:spring-boot-starter-jdbc:1.5.16.RELEASE \
--dependency mvn:com.h2database:h2:1.4.193 \
--open-api api/ob-api.json \
--logging-level org.apache.camel.k=DEBUG \
--resource camelk/config/schema.sql \
camelk/ob.java
```

To test the integration, use the following `curl` command:

```
curl http://ob-camelk-dev-openbanking.apps-crc.testing/open-data/banks/100
```

# Running the pipeline

## Pre-requisites:

- Tekton Operator installed (cluster-wide)


## Instructions

1. Create a new project where to run Pipelines:
	
	```
	oc new-project tekton-pipelines
	```

	Create ServiceAccount to interact with CamelK

	```
	oc apply -f tekton/camelk/pipeline-permissions.yaml
	```

	Create the Pipeline resources and definition:

	```
	oc apply -f tekton/common.yaml
	oc apply -f tekton/camelk/pipeline-definition.yaml
	```

1. Prepare DEV environment:

	Create DEV namespace:

	```
	oc new-project camelk-dev-openbanking
	```

	Grant `edit` permissions to the pipeline *ServiceAccount*:

	```
	oc policy add-role-to-user edit system:serviceaccount:tekton-pipelines:pipeline
	```

	Install CamelK Operator.

	Create an IntegrationPlatform

1. Prepare PROD environment:

	Create PROD namespace:

	```
	oc new-project camelk-prod-openbanking
	```

	Grant `edit` permissions to the pipeline *ServiceAccount*:

	```
	oc policy add-role-to-user edit system:serviceaccount:tekton-pipelines:pipeline
	```

	Install CamelK Operator.

	Create an IntegrationPlatform

1. Run the pipeline:

	```
	oc apply -f tekton/camelk/pipeline-run.yaml -n tekton-pipelines
	```

<br/>


# 3Scale Pre-requisites

The following instructions will prepare the environments with the necessary APICast deployments. Once the APICast instances are ready (staging/production), then the Tekton pipelines will be able to run successfully

## Prepare the Dev environment

1. Switch to the DEV environment

	```
	oc project camelk-dev-openbanking
    ```

1. Deploy (staging) APICast

	Ensure you create an access token in 3scale's web console. Once created, take note of the token and use it in the commands below.

	Create secret APICast uses to access 3Scale:

	```
	oc create secret generic apicast-url-secret-staging \
	--from-literal=AdminPortalURL=https://ACCESSS_TOKEN@rh-admin.3scale.net
	```

	where `ACCESS_TOKEN` represents the value of the 3Scale access token.

	Install the APICast Operator, navigate in the web console to the following location:

		OperatorHub -> APICast -> Subscribe

	Then create an APICast instance navigating to:

		Installed Operators -> APICast -> APICast -> Create APICast

   	using the following YAML definition:

	```yaml
	apiVersion: apps.3scale.net/v1alpha1
	kind: APIcast
	metadata:
	  name: staging
	  namespace: camelk-dev-openbanking
	spec:
	  adminPortalCredentialsRef:
	    name: apicast-url-secret-staging
	  pathRoutingEnabled: true
	  deploymentEnvironment: staging
    ```

    >*Note*: the field `pathRoutingEnabled` indicates APICast will route services based on `host+basepath`. This is important to allow a single OCP route to channel all defined services.

    The APICast staging instance needs to be exposed to external HTTP access. Run the following command to create an OpenShift route to allow access:

    ```
    oc expose service apicast-staging
    ```

1. Deploy (production) APICast

	```
	oc create secret generic apicast-url-secret-production \
	--from-literal=AdminPortalURL=https://ACCESSS_TOKEN@rh-admin.3scale.net
	```

	where `ACCESS_TOKEN` represents the value of the 3Scale access token.

	Create an APICast instance navigating to:

		Installed Operators -> APICast -> APICast -> Create APICast

   	using the following YAML definition:

	```yaml
	apiVersion: apps.3scale.net/v1alpha1
	kind: APIcast
	metadata:
	  name: live
	  namespace: camelk-dev-openbanking
	spec:
	  adminPortalCredentialsRef:
	    name: apicast-url-secret-production
	  pathRoutingEnabled: true
	  deploymentEnvironment: production
    ```

    The APICast staging instance needs to be exposed to external HTTP access. Run the following command to create an OpenShift route to allow access:

    ```
    oc expose service apicast-live
    ```

    <br/>

1. Install 3scale client:

	Mac:
	
		Dowload Package

			https://github.com/3scale/3scale_toolbox_packaging/releases/tag/v0.16.2

		Instructions

			https://github.com/3scale/3scale_toolbox_packaging#mac-os-x

2. Configure target 3scale system:

	Ensure an access token was previously created in 3scale's web console. Use the token in the command below:

	```
	3scale remote add 3scale-saas "https://ACCESS_TOKEN@rh-admin.3scale.net/"
	```

	where `ACCESS_TOKEN` represents the value of the 3Scale access token.

<br/>

# Manual API Deployment

1. Import an OpenAPI spec:

	<!-- 3scale import openapi -d 3scale-saas swagger.json --override-private-base-url=https://echo-api.3scale.net -t beer-catalog -->

	```
	3scale import openapi \
	-d 3scale-saas \
	../camelk-tekton/ob-api.json -t open-data --override-private-base-url=http://ob-camelk-dev-openbanking.apps-crc.testing:80 --production-public-base-url=http://apicast-example-apicast-camelk-dev-openbanking.apps-crc.testing:80 --default-credentials-userkey=dummy
	```



1. Create ApplicationPlan

	```
	3scale application-plan apply 3scale-saas open-data ob-plan -n "OB Test Plan" --default
	```

1. Create Application

	```
	3scale application apply 3scale-saas 1234567890abcdef --account=testaccount --name="OB Application" --description="Created from the CLI" --plan=ob-plan --service=open-data
	```

	The user key to access the API is `1234567890abcdef`

1. Promote to production

	3scale proxy-config promote 3scale-saas open-data


# Tekton Pipeline deployment

1. Create Secret with token access and Admin URL

	From the namespace where Pipelines will run, create the secret:

	```
	oc create secret generic 3scale-toolbox -n tekton-pipelines --from-file="$HOME/.3scalerc.yaml"
	```
